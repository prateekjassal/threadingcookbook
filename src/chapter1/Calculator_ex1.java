package chapter1;

/**
 * Created by prateek on 20/08/16.
 */

/**
 * Creating threads using Runnable and passing instance to a new thread
 */

public class Calculator_ex1 implements Runnable {
    private int base;

    public Calculator_ex1(int num) {
        this.base = num;
    }

    public void printTables() {
        for(int i=1;i<=10;i++) {
            System.out.printf("\n%s %d * %d = %d", Thread.currentThread().getName(),base,i,base*i);
        }
    }

    @Override
    public void run() {
        printTables();
    }

    public static void main(String[] args) {
        for(int i=1;i<=10;i++) {
            Thread t = new Thread(new Calculator_ex1(i));
            t.start();
        }
    }
}
