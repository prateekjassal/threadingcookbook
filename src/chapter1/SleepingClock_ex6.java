package chapter1;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by prateek on 21/08/16.
 */
public class SleepingClock_ex6 implements Runnable {


    @Override
    public void run() {
        for(int i=0;i<10;i++) {
            printTimeAndSleep();
        }
    }


    private void printTimeAndSleep() {
        Date date = new Date();
        System.out.printf("Date is : %s\n",date);
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException ie){
            System.out.println("Resumed ...");
        }
    }

    public static void main(String[] args) {
        SleepingClock_ex6 sleepingClock_ex6 = new SleepingClock_ex6();
        Thread t = new Thread(sleepingClock_ex6);
        t.start();
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        t.interrupt();

    }
}


/**  Output
 *
 * Gap between second and third entries is 2 seconds instead of 3 because the sleep was interrupted
 * The exception was caught and this resumed the thread back from sleep
 *
 Date is : Sun Aug 21 13:53:30 IST 2016
 Date is : Sun Aug 21 13:53:33 IST 2016
 Resumed ...
 Date is : Sun Aug 21 13:53:35 IST 2016
 Date is : Sun Aug 21 13:53:38 IST 2016
 Date is : Sun Aug 21 13:53:41 IST 2016
 Date is : Sun Aug 21 13:53:44 IST 2016
 Date is : Sun Aug 21 13:53:47 IST 2016
 Date is : Sun Aug 21 13:53:50 IST 2016
 Date is : Sun Aug 21 13:53:53 IST 2016
 Date is : Sun Aug 21 13:53:56 IST 2016
 *
 */





