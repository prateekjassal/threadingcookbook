package chapter1;

/**
 * Created by prateek on 20/08/16.
 */


/**
 *
 * Creating threads extending thread class
 *
 */

public class Calculator_ex2 extends Thread {
    private int base;

    public Calculator_ex2(int num){
        this.base = num;
    }

    public void printTables() {
        for(int i=1;i<=10;i++) {
            System.out.printf("%s %d * %d = %d\n",Thread.currentThread().getName(), base, i, base*i);
        }
    }

    public void run(){
        printTables();
    }

    public static void main(String[] args) {
        for(int i=1;i<=10;i++) {
            Calculator_ex2 calculator_ex2 = new Calculator_ex2(i);
            calculator_ex2.start();
        }
    }

}
