package chapter1;

import java.util.concurrent.TimeUnit;

/**
 * Created by prateek on 20/08/16.
 */

/**
 * Generating an interrupt for a non daemon thread and handling the interrupt to shutdown the thread
 */
public class PrimeGenerator_ex4 implements Runnable {

    public PrimeGenerator_ex4(){

    }

    private void generatePrimes() {
        int num = 2;
        while (true) {
            if(isPrime(num))
                System.out.printf("Prime number: %d\n",num);
            num++;
            if(Thread.currentThread().isInterrupted()) {
                System.out.println("PG thread interrupted");
                return;
            }
        }

    }

    private boolean isPrime(int num) {
        if(num<=2)
            return true;
        for(int i=2;i<=Math.sqrt(num);i++) {
            if(num % i ==0 )
                return false;
        }
        return true;
    }

    @Override
    public void run() {
        generatePrimes();
    }

    public static void main(String[] args) {
        PrimeGenerator_ex4 primeGenerator_ex4 = new PrimeGenerator_ex4();
        Thread t = new Thread(primeGenerator_ex4);
        t.start();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Interrupting prime number generator thread");
        t.interrupt();
    }
}
