package chapter1;


import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by prateek on 21/08/16.
 */

/**
 * Multiple threads created using the Runnable object share the variables of that Runnable class.
 * Treat all attributes as shared attributes.
 */

public class UnsafeThreadSharing_ex9 {
    public static void main(String[] args) {
        DatePicker_Safe datePicker = new DatePicker_Safe();
        for(int i=0;i<5;i++) {
            Thread t = new Thread(datePicker);
            t.start();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.printf("Done\n");
    }
}

class DatePicker implements Runnable {
    Date date;

    public DatePicker() {

    }


    @Override
    public void run() {
        date = new Date();
        System.out.printf("Start - Date chosen by thread %s\n",date);
        try {
            TimeUnit.SECONDS.sleep((int)Math.rint(Math.random()*10));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("End - Date chosen by thread %s\n",date);
    }
}

/**
 * Output
 *
 * At end of each thread, date ends up being the same while it is different at the beginning
 * This happens since all of them are sharing the same runnable object which has a single date variable
 *
 *
 */
