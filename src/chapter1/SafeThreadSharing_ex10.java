package chapter1;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by prateek on 21/08/16.
 */

/**
 *
 *
 *
 *
 * Why instance is defined at class level and not within the run method ?
 * Since the runnable object is being shared by multiple threads, the same ThreadLocal<T> reference defined
 * at class level is accessible to all threads.
 * If the instance is defined within the run method, each execution leads to creation of a new instance
 * and since the old threads had set their date in the older instance, they receive null when trying
 * to retrieve from this instance. Only the latest thread is able to retrieve its value.
 *
 * Since all threads do end up using the same reference, having it non static would have a different instance
 * for a different runnable object. Hence it makes sense to make it static.
 *
 */


public class SafeThreadSharing_ex10 {
    public static void main(String[] args) {
        DatePicker_Safe datePicker = new DatePicker_Safe();
        for (int i = 0; i < 5; i++) {
            Thread t = new Thread(datePicker);
            t.start();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.printf("Done\n");
    }
}

class DatePicker_Safe implements Runnable {

    private static ThreadLocal<Date> date = new ThreadLocal<Date>();

    public DatePicker_Safe() {

    }

    @Override
    public void run() {
//        date = new ThreadLocal<Date>();
        date.set(new Date());
        System.out.printf("Start - Date chosen by thread %s = %s %s\n", Thread.currentThread().getId(),date.get(), date.hashCode());
        try {
            TimeUnit.SECONDS.sleep((int) Math.rint(Math.random() * 10));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("End - Date chosen by thread %s = %s %s\n", Thread.currentThread().getId(),date.get(),date.hashCode());
    }
}