package chapter1;

/**
 * Created by prateek on 21/08/16.
 */

/**
 *
 * Capturing unchecked exceptions for a thread.
 * A class that implements this UncaughtExceptionHanlder gets a reference to the thread and the
 * stack trace.
 * Each thread has a method to set an uncaught exception handler to which an instance of the
 * above class can be set.
 */

public class ThreadUncheckedExceptionHandler_ex8 {
    public static void main(String[] args) {
        NumberParser numberParser = new NumberParser("Test");
        Thread t = new Thread(numberParser);
        t.setUncaughtExceptionHandler(new ExceptionHandler());
        t.start();
    }
}

class ExceptionHandler implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        System.out.printf("Exception caught for thread = %s\n", t.getName());
        System.out.printf("Stack trace = %s\n", e);
    }
}

class NumberParser implements Runnable {

    int num;
    String str;

    public NumberParser(String num){
        this.str = num;
    }

    @Override
    public void run() {
        num = Integer.parseInt(str);
    }
}