package chapter1;

import java.io.File;
import java.io.InterruptedIOException;
import java.util.List;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Created by prateek on 20/08/16.
 */

/**
 *
 * Interrupting a thread in a recursive program by throwing InterruptedException multiple levels up
 *
 */

public class RecursiveFileSearcher_ex5 implements Runnable {

    private String path;
    private String fileName;

    public RecursiveFileSearcher_ex5(String path, String fileName) {
        this.path = path;
        this.fileName = fileName;
    }

    @Override
    public void run() {
        File f = new File(path);
        if (f.isDirectory()) {
            try {
                processDirectory(f);
            } catch (InterruptedException e) {
                System.out.println("Search was interrupted");
            }
        }
    }

    private void processDirectory(File directory) throws InterruptedException {
        File[] files = directory.listFiles();
        if(files!=null) {
            for(File file: files) {
                if(file.isDirectory())
                    processDirectory(file);
                else
                    processFile(file);

            }
        }
        if(Thread.currentThread().isInterrupted()) {
            throw new InterruptedException();
        }
    }

    private void processFile(File file) throws InterruptedException {
        System.out.printf("Scanning file: %s\n",file.getAbsolutePath());
        if(file.getName().equals(fileName)) {
            System.out.printf("Found file %s\n", file.getAbsolutePath());
            throw  new InterruptedException();
        }
        if(Thread.currentThread().isInterrupted()) {
            throw new InterruptedException();
        }
    }

    public static void main(String[] args) {
        RecursiveFileSearcher_ex5 recursiveFileSearcher_ex5 = new RecursiveFileSearcher_ex5("/Users/prateek/Downloads","Torrent Downloaded From ExtraTorrent.cc.txt");
        Thread searcher = new Thread(recursiveFileSearcher_ex5);
        searcher.start();

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        searcher.interrupt();

    }

}
