package chapter1;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by prateekjassal on 22/8/16.
 */


/**
 *  Interrupting remaining threads in a thread group as soon as one thread completes its task
 */

public class ThreadGroup_ex10 {
    private static final int THREAD_COUNT = 10;
    public static void main(String[] args) {

        SearchTask searchTask = new SearchTask();
        ThreadGroup threadGroup = new ThreadGroup("SearchTaskGroup");
        // Initialize all threads in the thread group
        for(int i=0;i<THREAD_COUNT;i++) {
            Thread t = new Thread(threadGroup, searchTask);
            t.start();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.printf("Number of active threads = %d\n",threadGroup.activeCount());
        threadGroup.list();

        Thread threads[] = new Thread[threadGroup.activeCount()];
        threadGroup.enumerate(threads);

        for(Thread thread:threads) {
            System.out.printf("Thread name = %s, state = %s\n", thread.getName(), thread.getState());
        }
        waitFinish(threadGroup);
        threadGroup.interrupt();
        System.out.printf("Winning thread is %s\n", searchTask.getName());
    }

    /**
     * Keep waiting until all threads are alive
     */
    private static void waitFinish(ThreadGroup threadGroup) {
        while (threadGroup.activeCount() == THREAD_COUNT) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


class SearchTask implements Runnable {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    @Override public void run() {
        try {
            performTask();
            name = Thread.currentThread().getName();
        } catch (InterruptedException e) {
            System.out.printf("Thread interrupted with name = %s\n",Thread.currentThread().getName());
            return;
        }

    }

    private void performTask() throws InterruptedException {
        Random random = new Random(new Date().getTime());
        int value = (int) (random.nextDouble() * 100);
        TimeUnit.SECONDS.sleep(value);
    }

}


/**
 * Output
 *
 *
 * Number of active threads = 10
 java.lang.ThreadGroup[name=SearchTaskGroup,maxpri=10]
 Thread[Thread-0,5,SearchTaskGroup]
 Thread[Thread-1,5,SearchTaskGroup]
 Thread[Thread-2,5,SearchTaskGroup]
 Thread[Thread-3,5,SearchTaskGroup]
 Thread[Thread-4,5,SearchTaskGroup]
 Thread[Thread-5,5,SearchTaskGroup]
 Thread[Thread-6,5,SearchTaskGroup]
 Thread[Thread-7,5,SearchTaskGroup]
 Thread[Thread-8,5,SearchTaskGroup]
 Thread[Thread-9,5,SearchTaskGroup]
 Thread name = Thread-0, state = TIMED_WAITING
 Thread name = Thread-1, state = TIMED_WAITING
 Thread name = Thread-2, state = TIMED_WAITING
 Thread name = Thread-3, state = TIMED_WAITING
 Thread name = Thread-4, state = TIMED_WAITING
 Thread name = Thread-5, state = TIMED_WAITING
 Thread name = Thread-6, state = TIMED_WAITING
 Thread name = Thread-7, state = TIMED_WAITING
 Thread name = Thread-8, state = TIMED_WAITING
 Thread name = Thread-9, state = TIMED_WAITING
 Winning thread is Thread-5
 Thread interrupted with name = Thread-1
 Thread interrupted with name = Thread-0
 Thread interrupted with name = Thread-4
 Thread interrupted with name = Thread-2
 Thread interrupted with name = Thread-3
 Thread interrupted with name = Thread-9
 Thread interrupted with name = Thread-8
 Thread interrupted with name = Thread-7
 Thread interrupted with name = Thread-6

 *
 *
 *
 *
 *
 */


