package chapter1;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by prateek on 20/08/16.
 */

/**
 * Monitoring thread states and lifecycle, modifying thread priority and thread names
 */


public class Calculator_ex3 implements Runnable {
    private int base;

    public Calculator_ex3(int num) {
        this.base = num;
    }

    public void printTables() {
        for(int i=1;i<=10;i++) {
            System.out.printf("\n%s %d * %d = %d", Thread.currentThread().getName(),base,i,base*i);
        }
    }

    @Override
    public void run() {
        printTables();
    }

    public static void main(String[] args) {

        Thread threads[] = new Thread[10];
        Thread.State threadStates[] = new Thread.State[10];

        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/prateek/ThreadingCookbook/chapter1/threadStates.txt"))) {
            for(int i=1;i<=10;i++) {
                threads[i-1] = new Thread(new Calculator_ex1(i));
                threads[i-1].setName("Thread "+(i-1));
                if((i-1)%2 == 0) {
                    threads[i-1].setPriority(Thread.MAX_PRIORITY);
                } else {
                    threads[i-1].setPriority(Thread.MIN_PRIORITY);
                }
                threadStates[i-1] = threads[i-1].getState();
                bufferedWriter.write(String.format("Main : Status of thread %s = %s\n", threads[i-1].getName(),threadStates[i-1]));
                threads[i-1].start();
            }

            boolean finish = false;
            while(!finish) {
                finish = true;
                for(int i=1;i<=10;i++) {
                    // If the state changed, log it to the file
                    if(threadStates[i-1]!= threads[i-1].getState()) {
                        logThreadStateChange(bufferedWriter, threadStates[i-1], threads[i-1]);
                        threadStates[i-1] = threads[i-1].getState();
                    }
                    finish = finish && threadStates[i-1]== Thread.State.TERMINATED;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void logThreadStateChange(BufferedWriter bufferedWriter, Thread.State threadState, Thread t) throws IOException {
        bufferedWriter.write(String.format("Main : Thread name = %s\n", t.getName()));
        bufferedWriter.write(String.format("Main : Thread priority = %s\n", t.getPriority()));
        bufferedWriter.write(String.format("Main : Old state of thread = %s\n", threadState));
        bufferedWriter.write(String.format("Main : New state of thread = %s\n", t.getState()));
    }
}
