package chapter1;

import java.util.concurrent.TimeUnit;

/**
 * Created by prateek on 21/08/16.
 */

/**
 * Waiting for a called thread to be finalized before the calling thread can continue execution
 * Join is used for this, main thread waits for the spawned threads to finish if join is invoked
 * on these thread instances in the main thread
 *
 */
public class ThreadFinalization_ex7 {

    public static void main(String[] args) {
        Thread t1 = new Thread(new DataSourceLoader());
        Thread t2 = new Thread(new NetworkResourceLoader());
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Main thread finished execution");

    }

}

class DataSourceLoader implements Runnable {

    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Loaded data resources");
    }
}

class NetworkResourceLoader implements Runnable{

    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(6);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Loaded network resources");
    }
}

/**
 * Output
 *
 * Without join
 *
 Main thread finished execution
 Loaded data resources
 Loaded network resources

 With join only on DataResources thread from main

 Loaded data resources
 Main thread finished execution
 Loaded network resources

 With join on both threads from main thread

 Loaded data resources
 Loaded network resources
 Main thread finished execution

 *
 *
 */

